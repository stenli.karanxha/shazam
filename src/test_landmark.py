################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Unit Tests
#  author:      Antonio De la Torre, Stenli Karanxha
#  summary:     Define the unit tests for the landmarking of an fft
#  revision:    1.1, 22 Jan 2014 SK defined list and framework for the tests.
#               1.2, 31 Jan 2014 SK completed and passed the tests.
################################################################

import unittest
import json
import os

import landmark
import common

class ResultsKeywords():
    ROUNDING_FACTOR = "rounding_factor"
    BLOCK_SIZE = "block_size"
    AVERAGE_DENSITY = "average_density"
    FFT_MATRIX="fft_matrix"
    INDEXES="indexes"
    LANDMARKS="landmarks"
    ALL = [ROUNDING_FACTOR,  BLOCK_SIZE,AVERAGE_DENSITY,FFT_MATRIX, INDEXES,  LANDMARKS ]


class TestLandmark(unittest.TestCase):

    def setUp(self):
        pass
        
    def tearDown(self):
        pass
        
    def test_mono_01_landmark(self):
        """ Test landmark of a monotone audio sample: 10 kHz for 0.5 s, sample rate 44.1 kHz """
        self._check_landmark('mono01')
        
    def test_mono_02_landmark(self):
        """ Test landmark of a monotone audio sample: 12 kHz for 0.5 s, sample rate 48 kHz """
        self._check_landmark('mono02')

    def test_chirp_01_landmark(self):
        """ Test landmark of a chirp audio sample: 500 Hz to 15 kHz linearly in  0.01 s, sample rate 44.1 kHz """
        self._check_landmark('chirp01')

    def test_chirp_02_landmark(self):
        """ Test landmark of a chirp audio sample: 500 Hz to 15 kHz logarithmic in  0.01 s, sample rate 48 kHz """
        self._check_landmark('chirp02')

    def test_random_point_landmark(self):
        """ Identify the single valid landmark point. """
        self._check_landmark('random_point') 
        
    def test_random_sparse_landmark(self):
        """ Identify valid landmark points in a sparse file (Less local maxima than the required density). """
        self._check_landmark('random_sparse')         
        
    def test_random_dense_landmark(self):
        """ Identify valid landmark points in a dense file (More local maxima than the required density). """
        self._check_landmark('random_dense')                 
        
    def _check_landmark(self,  base_file_name):
        """ Compare the output of the landmark algorithm with the data written in the results file.
        """
        # Reads the results file
        result_data = None
        self._prepare_file(base_file_name)
        if os.path.exists(self.result_file_name):
            with open(self.result_file_name) as data_file:
                try:
                    result_data = json.load(data_file)
                except:
                    self.assertTrue(False, 'The result json file is not valid.')
        self.assertIsNotNone(result_data, \
            'The json file with the result data is invalid.')
        self.assertTrue(self._is_result_data_valid(result_data), \
            'The json file with the result data does not contain all the keys.')
        # Prepares the algorithm
        rounding_factor = result_data[ResultsKeywords.ROUNDING_FACTOR]
        time_interval = result_data[ResultsKeywords.BLOCK_SIZE]
        average_density = result_data[ResultsKeywords.AVERAGE_DENSITY]
        my_algorithm = landmark.LandmarkAlgorithm(rounding_factor,  time_interval,  average_density)
        # Runs 
        discrete_spectrum = result_data[ResultsKeywords.FFT_MATRIX]
        indexes = my_algorithm.calculate_landmark_indexes(discrete_spectrum)
        expected_indexes = self._create_indexes_list(result_data[ResultsKeywords.INDEXES])
        self.assertTrue(self._compare_list(expected_indexes, indexes), \
            "The indexes of the landmarks do not corrispond.")
        
    def _prepare_file(self,  base_file_name):
        self.result_file_name = self._get_result_file_name(base_file_name)
        self.assertTrue(os.path.exists(self.result_file_name))    
        
    @staticmethod
    def _create_landmarks_list(coordinate_list):
        return [common.Landmark(coord[0],  coord[1]) for coord in coordinate_list]
        
    @staticmethod
    def _create_indexes_list(coordinate_list):
        return [(coord[0],  coord[1]) for coord in coordinate_list]        
        
    @staticmethod
    def _is_result_data_valid(result_data):
        return all([key in result_data.keys() for key in ResultsKeywords.ALL])
        
    @staticmethod
    def _get_result_file_name(base_file_name):
        return '../files/unit_test_data/' + str(base_file_name) + '.json'
        
    @staticmethod
    def _compare_list(list_1, list_2):
        return list_1!=None and list_2!=None and  all([elem in list_2 for elem in list_1]) and all([elem in list_1 for elem in list_2])

def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestLandmark)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
