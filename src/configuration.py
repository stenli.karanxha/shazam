################################################################
#  org.: 	            UZH
#  project: 	        MAT101, Programming in Python.
#		                     Z - Shazam
#  component:	    Configuration
#  author: 	        Stenli Karanxha
#  summary:	        Defines classes for managing the configuration
#  revision: 	        1.1, 17 Jan 2014
################################################################

import os
import json

class CDefaults(object):
    """  Contains the default configuration data """
    DATABASE_FILE_NAME = ('database_file_name',  '../files/program_data/simple_database.json')
    TIME_WINDOW = ('time_window',  1)
    FFT_WINDOW_SIZE = ('fft_window_size',  2048)
    ROUNDING_FACTOR = ('rounding_factor',  (2,  -2))
    BLOCK_SIZE = ('block_size',  30)
    AVERAGE_DENSITY = ('average_density',  9)
    MIN_QUALITY_FACTOR = ('min_quality_factor',  10)
    
    ALL = {DATABASE_FILE_NAME,  TIME_WINDOW,  FFT_WINDOW_SIZE, \
                ROUNDING_FACTOR,  BLOCK_SIZE,  AVERAGE_DENSITY,  MIN_QUALITY_FACTOR}


class Configuration(object):
    """ Contains all the information needed to initialize the level."""
    
    def __init__(self,  file_name):
        """ Constructor with argument:
           file_name - full path of the configuration file.
        """
        self._file_name = file_name
        self.data = {}
        
    def load(self):
        """ Load the configuration from a file, if it is valid. Missing fields are set to default."""
        import os
        self.data = {}
        if os.path.exists(self._file_name):
            with open(self._file_name,  'r') as data_file:
                self.data = json.load(data_file)
        for default in CDefaults.ALL:
            if not default[0] in self.data:
                self.data[default[0]] = default[1]
                
    def save(self):
        """ Save the configuration to a file, overwriting it if existing."""
        with open(self._file_name,  'w') as data_file:
            json.dump(self.data, data_file, sort_keys = True, indent = 4, ensure_ascii=False)
            
    def set(self,  key,  value):
        self.data[key] = value
        
    def get(self,  key):
        if key in self.data:
            return self.data[key]
        return None

    def is_valid(self):
        '''Checks whether all necessary keywords are present in the json-object '''
        return all([default[0] in self.data.keys() for default in CDefaults.ALL]) 
