################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Unit Tests
#  author:      Lea Lutz, Regula Baettig, Stenli Karanxha
#  summary:     Defines the unit tests for the database managing classes
#  revision:    1.1, 23 Jan 2014, SK defined TestSetup and test structure.
#               1.2, 25 Jan 2014, LL & RB completed and passed unit tests.
################################################################

import unittest
import os
import shutil
import json

import database

class ResultsKeywords():
    RECORD_NUMBER = 'record_number'
    AFTER_ADD_RECORD_NUMBER = 'after_add_record_number'
    HASH_NUMBER = 'hash_number'
    RECORD_ID = 'record_id'
    RECORD = 'record'
    ADD_RECORD = 'additional_record'
    HASH = 'hash'
    RECORD_ID_FROM_HASH = 'record_id_from_hash'
    ALL = [RECORD_NUMBER, AFTER_ADD_RECORD_NUMBER, \
            HASH_NUMBER, RECORD_ID, RECORD, ADD_RECORD, HASH, RECORD_ID_FROM_HASH]


class TestSetup(object):
    """
        Contains all the information to set up the test for a single database type.
        Member variables:
        database -- database object under test
        file_name -- string, full path of the database file
        backup_file_name -- string, full path of the backup of the database.
        result_file_name -- string, full path of the file with the expected results.
    """
    
    def __init__(self,  descriptor):
        """
        Constructor with argument:
        desriptor - an element of the list of descriptors.
        """
        self.database_object = descriptor[0]
        
        base_file_name = descriptor[1]
        self.database_file_name = self._get_database_file_name(base_file_name)
        self.backup_file_name = self._get_database_backup_file_name(base_file_name)
        
        self.result_data = None
        self.result_file_name = self._get_results_file_name(base_file_name)
        if os.path.exists(self.result_file_name):
            with open(self.result_file_name) as data_file:
                try:
                    self.result_data = json.load(data_file)
                except:
                    self.result_data = None
        for keyword in ResultsKeywords.ALL:
            if not keyword in self.result_data:
                self.result_data[keyword] = None
        
    def is_valid(self):
        """ Valid if the database and result data are valid. """
        return os.path.exists(self.database_file_name) and \
            not self.result_data is None and \
            all([key in self.result_data.keys() for key in ResultsKeywords.ALL])
        
    def is_backup_valid(self):
        """ Valid if the backup file is valid. """
        return os.path.exists(self.backup_file_name)
        
    def backup_database(self):
        """
        Backups the database.
        """
        if os.path.exists(self.database_file_name):
            shutil.copy2(self.database_file_name, self.backup_file_name)
        
    def reset_database(self):
        """
        Resets the database to its backup version. Deletes the backup.
        """
        if os.path.exists(self.backup_file_name):
            shutil.copy2(self.backup_file_name, self.database_file_name)
            os.remove(self.backup_file_name)
        
    @staticmethod
    def _get_database_file_name(base_file_name):
        return '../files/unit_test_data/' + str(base_file_name)
        
    @staticmethod
    def _get_database_backup_file_name(base_file_name):
        return '../files/unit_test_data/' + str(base_file_name) + '.bck'

    @staticmethod  
    def _get_results_file_name(base_file_name):
        return '../files/unit_test_data/' + str(base_file_name) + '.json'


descriptors = [(database.DatabaseInterface(),  'DatabaseDummy'),
               (database.DatabaseMemory(),  'DatabaseMemory')
                # (database.DatabaseDisk(), 'DatabaseDisk')
            ]

test_setups = [TestSetup(descriptor) for descriptor in descriptors]


class TestDatabase(unittest.TestCase):

    def setUp(self):
        """ Set up of the unit tests. Backups the databases."""
        for setup in test_setups:
            self.assertTrue(setup.is_valid())
            setup.backup_database()
            self.assertTrue(setup.is_backup_valid())
        
    def tearDown(self):
        """ Tear down of the unit tests. Resets the databases."""
        for setup in test_setups:
            setup.reset_database()
            self.assertFalse(setup.is_backup_valid())
            
    def test_print_record(self):
        """ The record dictionary can be correctly printed. """
        metadata = {"album": "Wednesday Morning, 3 AM", "author": "Simon & Garfunkel", "year": 1964, "title": "You Can Tell the World"}
        landmarks = []
        my_record = database.construct_record(metadata,  landmarks)
        result = database.record_metadata_to_string(my_record)
        self.assertTrue(result != None and len(result) > 0)
        print result

    def test_open_close(self):
        """ It's possible to open / close a database. """
        for setup in test_setups:
            setup.database_object.open(setup.database_file_name)
            self.assertTrue(setup.database_object.is_opened())
            setup.database_object.close()
            self.assertFalse(setup.database_object.is_opened())
    
    def test_record_number(self):
        """ It's possible to read the number of records from the database. """
        for setup in test_setups:
            expected_records = setup.result_data[ResultsKeywords.RECORD_NUMBER]
            setup.database_object.open(setup.database_file_name)
            effective_number = setup.database_object.number_of_records()
            self.assertEqual(expected_records,  effective_number)
    
    def test_hash_number(self):
        """ It's possible to read the number of hashes from the database. """
        for setup in test_setups:
            expected_hash = setup.result_data[ResultsKeywords.HASH_NUMBER]
            setup.database_object.open(setup.database_file_name)
            effective_number = setup.database_object.number_of_hash()
            self.assertEqual(expected_hash,  effective_number)   
            
    def test_reset(self):
        """ It's possible to reset the database. """
        for setup in test_setups:
            setup.database_object.open(setup.database_file_name)
            setup.database_object.reset()
            effective_number = setup.database_object.number_of_records()
            self.assertEqual(effective_number,  0)
    
    def test_add_record(self):
        """ It's possible to add a record to the database and read it back. """
        for setup in test_setups:
            added_record = setup.result_data[ResultsKeywords.ADD_RECORD]
            setup.database_object.open(setup.database_file_name)
            id = setup.database_object.add_record(added_record)
            if id:
                read_record = setup.database_object.get_record(id)
                self.assertEqual(added_record,  read_record)
            
    def test_remove_record(self): 
        """ It's possible to remove a record from the database. """
        for setup in test_setups:
            remove_record_id = setup.result_data[ResultsKeywords.RECORD_ID]
            setup.database_object.open(setup.database_file_name)
            self.assertTrue(setup.database_object.remove_record(remove_record_id))
            read_record = setup.database_object.get_record(id)
            self.assertIsNone(read_record)            
            
    def test_save(self):
        """ It's possible to save a change in the database, and it stays coherent. """
        for setup in test_setups:
            # Opens the database, and checks it's coherent.
            setup.database_object.open(setup.database_file_name)
            self.assertTrue(setup.database_object.is_coherent())
            # Adds the record
            added_record = setup.result_data[ResultsKeywords.ADD_RECORD]
            id = setup.database_object.add_record(added_record)
            # Closes / reopens the database
            setup.database_object.close()
            setup.database_object.open(setup.database_file_name) 
            self.assertTrue(setup.database_object.is_coherent())
            # Checks that the 
            expected_number = setup.result_data[ResultsKeywords.AFTER_ADD_RECORD_NUMBER]
            effective_number = setup.database_object.number_of_records()
            self.assertEqual(expected_number, effective_number)
            
    def test_get_record(self):
        """ It's possible to read a record, given the record ID. """
        for setup in test_setups:
            expected_record = setup.result_data[ResultsKeywords.RECORD]
            required_record_id = setup.result_data[ResultsKeywords.RECORD_ID]
            setup.database_object.open(setup.database_file_name)
            actual_record = setup.database_object.get_record(required_record_id)
            self.assertEqual(expected_record,  actual_record)
            
    def test_get_record_ids_with_hash(self):
        """ It's possible to search for a hash value,  """
        for setup in test_setups:
            search_hash = setup.result_data[ResultsKeywords.HASH]
            resulting_hash_list = setup.result_data[ResultsKeywords.RECORD_ID_FROM_HASH]
            setup.database_object.open(setup.database_file_name)
            actual_hash_list = setup.database_object.get_record_ids_with_hash(search_hash)
            self.assertIsNotNone(resulting_hash_list)
            self.assertIsNotNone(actual_hash_list)
            self.assertEqual(resulting_hash_list,  actual_hash_list)
            
    def test_coherence_record_number(self):
        """ The information 'number of records' is coherent to the number of records. """
        for setup in test_setups:
            setup.database_object.open(setup.database_file_name)
            mydatabase = setup.database_object
            record_number = mydatabase.number_of_records()
            counter_records = len(mydatabase.get_records())
            self.assertEqual(record_number, counter_records)
            
    def test_coherence_hash_number(self):
        """ The information 'number of hashes' is coherent to the number of hashes. """
        for setup in test_setups:
            setup.database_object.open(setup.database_file_name)
            mydatabase = setup.database_object
            hash_number = mydatabase.number_of_hash()
            counter_hash = len(mydatabase.get_hashes())
            self.assertEqual(hash_number, counter_hash)
            
    def test_coherence_hash_records(self):
        """ The list of hashes saved for each record is coherent to the hash table."""
        for setup in test_setups:
            setup.database_object.open(setup.database_file_name)
            mydatabase = setup.database_object
            all_records = mydatabase.get_records()
            all_hashes = mydatabase.get_hashes()
            for record_id in all_records:
                for hash in mydatabase.get_hashes_for_record_id(record_id):
                    record_ids_for_hash = all_hashes[hash]
                    self.assertIn(record_id,  record_ids_for_hash)
         
def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDatabase)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
