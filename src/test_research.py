################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Unit Tests
#  author:      Stenli Karanxha
#  summary:     Define the unit tests for the research of a sample.
#  revision:    1.1, 24 Jan 2014
################################################################

import research
import unittest

class TestResearch(unittest.TestCase):

    def setUp(self):
        pass
        
    def tearDown(self):
        pass

    def test_mytest(self):
        self.assertTrue(True)
    
def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestResearch)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
