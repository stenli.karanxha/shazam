################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Fingeprinting
#  author:      Antonio De la Torre, Stenli Karanxha,
#  summary:     Generates a raw landmark based on the FFT of an audio file.
#  revision:    1.1, 22 Jan 2014, SK defined interface
#               1.2, 25 Jan 2014, AT completed part LandmarkAlgorithm and passed base tests.
#               1.3, 31 Jan 2013, SK completed the point quality calculation and tests.
################################################################

import numpy as np

import common
import hash

class LandmarkAlgorithm(object):
    
    ############
    # Public interface  
    ############
    
    def __init__(self,  rounding_factor,  block_size,  average_density):
        """ Constructor with arguments: 
        rounding_factor -- Maximum number of digits allowed for time and frequency.
        block_size -- Time interval on which the landmark density will be calculated.
        average_density -- Average density of the landmark points.
        """
        self._rounding_factor = rounding_factor
        self._block_size = block_size
        self._average_density = average_density
        self._hash_algorithm = hash.HashAlgorithm(rounding_factor)
        self._rows=0
        self._columns=0
        
    def calculate_landmark_indexes(self,  discrete_spectrum):
        """
            Calculate the indexes of the landmarks of a discrete spectrum. Used mainly
            for test, when the real landmark is not important, but it's position is.
            Input:
            discrete_spectrum -- matrix containing the spectrum of an audio signal with 
                time along the rows and frequency along the columns.
            Output:
            indexes -- List of couples, each representing the indexes of a Landmark.
        """
        if not self._check_sanity(discrete_spectrum):
            return None
        self._calculate_input_size(discrete_spectrum)
        point_quality = self._calculate_all_points_quality(discrete_spectrum)
        characteristics = self._calculate_spectrum_characteristics(point_quality)
        return characteristics[0]

    def calculate_landmarks(self,  discrete_spectrum):
        """
            Calculate the landmarks of a discrete spectrum. 
            Input:
            discrete_spectrum -- matrix containing the spectrum of an audio signal with 
                time along the rows and frequency along the columns.
            Output:
            landmarks -- List of Landmark objects.
        """
        if not self._check_sanity(discrete_spectrum):
            return None
        self._calculate_input_size(discrete_spectrum)
        point_quality = self._calculate_all_points_quality(discrete_spectrum)
        characteristics = self._calculate_spectrum_characteristics(point_quality)
        return characteristics[1]
    
    ############
    # Private interface
    ############
    @staticmethod 
    def _check_sanity(discrete_spectrum):
        """
        Check that the matrix is not empty and secondly that all the elements inside the 
        matrix have the same number of elements
        """
        return len(discrete_spectrum) > 0 and \
            all([len(elem)==len(discrete_spectrum[0]) for elem in discrete_spectrum])
    
    def _calculate_input_size(self,  discrete_spectrum):
        self.rows=len(discrete_spectrum)
        self.columns=len(discrete_spectrum[0])

    def _calculate_all_points_quality(self,  discrete_spectrum):
        """ Produce a matrix with the same dimensions as discrete_spectrum, assigning 
            to each point a quality factor."""
        matrix=np.zeros((self.rows,self.columns), np.float32)
        for i in range(self.rows):
            for j in range(self.columns):
                matrix[i][j]= self._calculate_single_point_quality(discrete_spectrum, i, j)
        return matrix
        
    def _calculate_single_point_quality(self,  discrete_spectrum,  row_index,  column_index):
        """ Calculate the quality factor of a single point. This identifies if a point is a good local
        maximum.  The meaning of the generated quality factors are:
            -1 -- not a local maximum
            > 0 -- local maximum. 
            The quality factor is given as a product of the two following quantities:
                the ratio between the maximum and the second biggest point
                the distance of the neighbours for which the point still stays a maximum.
        """
        # The power in the point under control
        base_power = discrete_spectrum[row_index][column_index]
        # Calculate the minimum ratio between the point and its neighbours. 
        MIN_POWER = 1e-6
        neighbours = self._get_neighbour_indexes((row_index,  column_index),  1)
        ratios = [base_power / max(discrete_spectrum[index[0]][index[1]],  MIN_POWER) for index in neighbours]
        min_ratio = min(ratios)
        if min_ratio < 1:   # If this is smaller than 1, then the point is not a local maximum.
            return -1
        # Calculates the extension of the maximum.
        extension = 1
        for distance in range(2,  self.rows):
            neighbours = self._get_neighbour_indexes((row_index,  column_index),  distance)
            if all([base_power > discrete_spectrum[index[0]][index[1]] for index in neighbours]):
                extension = distance
            else:
                break
        return min_ratio * extension        
        
    def _get_neighbour_indexes(self,  base_index,  distance):
        """ Produce the indexes of all the neighbour points.
            input:
                base_index -- couple,  indexes of a point in the discrete_spectrum matrix.
                distance -- int, distance of the required neighbours.
            Output:
                nearby_indexes -- list of indexes of the neighbours.
        """
        indexes = []
        assert(distance > 0)
        # Left
        fixed = base_index[1] - distance
        add_indexes = [(i,  fixed) for i in range(base_index[0] - distance,  base_index[0] + distance)]
        indexes.extend(add_indexes)
        # Right
        fixed = base_index[1] + distance
        add_indexes = [(i,  fixed) for i in range(base_index[0] - distance,  base_index[0] + distance)]
        indexes.extend(add_indexes)
        # Up
        fixed = base_index[0] - distance
        add_indexes = [(fixed,  i) for i in range(base_index[1] - distance + 1,  base_index[1] + distance - 1)]
        indexes.extend(add_indexes)
        # Down
        fixed = base_index[0] + distance
        add_indexes = [(fixed,  i) for i in range(base_index[1] - distance + 1,  base_index[1] + distance - 1)]
        indexes.extend(add_indexes)
        # Prepares and returns the valid indexes.
        clean_indexes = [index for index in indexes if self._is_index_valid(index)]
        return clean_indexes
        
    def _is_index_valid(self,  index):
        """ Return true if an index is valid."""
        return index[0] >= 0 and index[0] < self.rows and index[1] >= 0 and index[1] < self.columns

    def _calculate_spectrum_characteristics(self,  point_quality):
        """ Select the best quality points as landmark points, trying to maintain the average 
        density of points in each time interval constant and near the specification.
        """
        peak_indexes=[]
        landmarks = []
        max_num_blocks= int(self.columns/self._block_size)
        for i in range (max_num_blocks):
            # Borders of the block
            time_index_start = i*self._block_size
            time_index_end = min((i+1)*self._block_size, self.columns-1)
            # Calculates peak indexes within the block
            block_peak_indexes = self._get_block_peak_indexes(point_quality, time_index_start, time_index_end)
            peak_indexes.extend(block_peak_indexes)
            # Calculates landmarks within the block.
            block_landmarks = self._get_block_landmarks(block_peak_indexes)
            landmarks.extend(block_landmarks)
        return (peak_indexes,  landmarks)

    def _get_block_peak_indexes(self,  point_quality, time_index_start,  time_index_end):
        """
        Within the block of points identified from the start and end time indexes, find the highest 
        quality points, and return their indexes.
        """
        # Generates the acceptable points.
        acceptable_points=[]
        for i in range(self.rows):
            for j in range(time_index_start, time_index_end +1):
                if point_quality[i][j]>0:
                    acceptable_points.append((i, j, point_quality[i][j]))                    
        acceptable_points = self._sort_by_quality(acceptable_points)
        del acceptable_points[self._average_density:len(acceptable_points)]
        # Prepares the peak indexes, taking only the indexes out of the acceptable points
        peak_indexes = [(point[0],  point[1]) for point in acceptable_points]
        return peak_indexes
        
    def _get_block_landmarks(self, peak_indexes): 
        """Return a list of landmarks given a list of peak indexes within a block."""
        landmarks=[]
        max_iterator = len(peak_indexes)
        for i in range(max_iterator):
            peak_1 = self._get_spectrum_peak(peak_indexes[i])
            for j in range(i,  max_iterator):
                peak_2 = self._get_spectrum_peak(peak_indexes[j])
                time = (peak_1.get_time() + peak_2.get_time()) * 0.5
                hash = self._hash_algorithm.calculate_hash(peak_1,  peak_2)
                add_landmark = common.Landmark(time,  hash)
                landmarks.append(add_landmark)
        return landmarks

    def _get_spectrum_peak(self,  indexes):
        """
        Receive a couple of indexes (time_index, frequency_index) and using the quantization
        data produce a Landmark object.
        """
        time_quantization = 10**self._rounding_factor[0]
        time=indexes[0]*time_quantization
        frequency_quantization = 10**self._rounding_factor[1]
        frequency=indexes[1]*frequency_quantization
        return common.SpectrumPeak(time, frequency)

    @staticmethod
    def _sort_by_quality(points):
        """
        Receive a list of tuples (time index, frequency index, quality) and sorts it in order 
        of decreasing quality.
        """
        return sorted(points, key = lambda point_tuple: point_tuple[2],  reverse=True)

    @staticmethod
    def _sort_by_time(points):
        """
        Receive a list of tuples (time index, frequency index, quality) and sorts it in order 
        of increasing time index.
        """
        return sorted(points, key = lambda point_tuple: point_tuple[0])
        
