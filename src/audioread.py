# -*- coding: utf-8 -*-
"""
Created on Wed Jan 22 13:12:20 2014

@author: xenia
"""

################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   FFT transformation
#  author:      Xenia Ohmer
#  summary:     Produces the FFT of an audio file in wav format.
#  revision:    1.1, 26 Jan 2014
################################################################

import wave
import numpy as np
import struct
import matplotlib.pyplot as plt


class AudioPiece(object):
    
    ############
    # Public interface  
    ############
    
    def __init__(self, fft_windowsize = 1024):
        """ Constructor.
        """
        if is_power_of_2(fft_windowsize):
            self.windowsize = fft_windowsize
        else:
            self.windowsize = 1024
        self.fp = None
        self.samplerate = None
        self.samplecount = None
        
    def calculate_fft(self, wav):
        """ Calculates fft matrix of a wave file. 
        
        Input: AudioPiece, wave file
        Output: fft matrix of wave file.
        """ 
        
        self.fp = wave.open(wav,'rb')
        # sampling frequency
        self.samplerate = self.fp.getframerate()
        # number of audioframes
        self.samplecount = self.fp.getnframes() 
        data = self.readdata()
        fft_matrix = self.fft(data)
        #self.windowplot(data,fft_matrix)
        #self.fft_matrix_plot(fft_matrix)
        return fft_matrix
        
    ############
    # Private interface
    ############
        
    def readdata(self):
        """ Reads the data of the wave file and saves it as floats in a matrix.
        
        Input: Audiopiece
        Output: data of wav file as float matrix with one row for each 
        fft_window. 
        """        
        
        # create matrix with one row for each window
        fftcount = self.samplecount/self.windowsize -2
        data = np.zeros((fftcount,self.windowsize),dtype = float)
        samplewidth = self.fp.getsampwidth()
        for i in xrange(fftcount):
            # read fft_windowsize samples at a time
            tmp = self.fp.readframes(self.windowsize/samplewidth)
            # unpack binary values (as strings) to floats
            # -128 as normalisation as values range from 0-255
            data[i,:] = np.array(struct.unpack('%dB'%self.windowsize,tmp), \
            dtype = float) -128.0
        return data 
        
    def fft(self, data):
        """ Applies Hamming window to data and then performs a Fast Fourier 
        Transform.
        
        Input: Audiopiece, data
        Output: real fft matrix of data 
        """
        hammed = data * np.hamming(self.windowsize)
        fft_matrix = abs(np.fft.rfft(hammed, self.windowsize))
        fft_matrix = 10*np.log10(1e-20+fft_matrix)
        return fft_matrix
        
        
    def windowplot(self, data, fft, window = 2):
        """ Plots frequency and power of frequency of the data. 
        
        Input: Audiopiece, wav data, fft and number of the window to plot. 
        Plots frequency and power of frequency for given FFT-window."""
        
        #plot frequency for first window
        plt.plot(np.arange(self.windowsize),data[window,:])
        #plot power of frequency for first window
        plt.plot(np.arange(self.windowsize/2+1),fft[window,:])
        plt.xlabel('time')
        plt.ylabel('frequency')
        plt.title('Frequency and FFT-data of FFT-window %d'%window)
        plt.show()
            
    def fft_matrix_plot(self, fft_matrix):
        """ Plots fft matrix with color denoting power of frequency.
        
        Input: Audiopiece and fft-matrix.
        Plots rows of fft-matrix against columns of fft-matrix with the color
        denoting the power of the frequencies""" 
        
        fft_length = (self.windowsize/2)+1
        # arrange axes for plotting
        y = 0.5*float(self.samplerate)/fft_length * np.arange(fft_length)
        x = (self.samplecount/float(self.samplerate)) / ((self.samplecount/ \
        self.windowsize)-2)* np.arange((self.samplecount/self.windowsize)-2)
        
        plt.contourf(x,y, np.transpose(fft_matrix))
        plt.xlabel('time')
        plt.ylabel('frequency')
        plt.title('Power of Frequency at certain time')
        plt.show()
        
############################ utility functions ###############################

def is_power_of_2(x):
    return (x != 0) and ((x & (x - 1)) == 0)
    
def main():
    audio = AudioPiece()
    fft_matrix = audio.calculate_fft('../files/unit_test_data/sweep02.wav')

if __name__ == '__main__':
        main()
