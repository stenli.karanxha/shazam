################################################################
#  org.: 	            UZH
#  project: 	        MAT101, Programming in Python.
#		                     Z - Shazam
#  component:	    Unit Tests
#  author: 	        Stenli Karanxha
#  summary:	        Defines the unit tests for the configuration managing class
#  revision: 	        1.1, 17 Jan 2014
################################################################

from configuration import Configuration,  CDefaults
import unittest
import os
import json


CONFIG_EXIST_FILE_NAME = '../files/program_data/config.json'
CONFIG_ABSENT_FILE_NAME = '../files/program_data/config_killme.json'
KEY_FREQ = CDefaults.FREQUENCY_COUNT[0]
DATA_DEFAULT_FREQ = CDefaults.FREQUENCY_COUNT[1]
DATA_CONFIG_FREQ = 1024


class TestConfiguration(unittest.TestCase):

    def setUp(self):
        if os.path.exists(CONFIG_ABSENT_FILE_NAME):
            os.remove(CONFIG_ABSENT_FILE_NAME)
        
    def tearDown(self):
        if os.path.exists(CONFIG_ABSENT_FILE_NAME):
            os.remove(CONFIG_ABSENT_FILE_NAME)

    def test_load_valid(self):
        """ Load a valid configuration file, and check that the file data was properly read."""
        my_configuration = Configuration(CONFIG_EXIST_FILE_NAME)
        my_configuration.load()
        self.assertIsNotNone(my_configuration.data)
        self.assertTrue(my_configuration.is_valid())
        self.assertEqual(my_configuration.get(KEY_FREQ),  DATA_CONFIG_FREQ)
    
    def test_load_invalid(self):
        """ Load an invalid configuration file, and check that the default data was properly read."""
        my_configuration = Configuration(CONFIG_ABSENT_FILE_NAME)
        my_configuration.load()
        self.assertIsNotNone(my_configuration.data)
        self.assertTrue(my_configuration.is_valid())
        self.assertEqual(my_configuration.get(KEY_FREQ),  DATA_DEFAULT_FREQ)
    
    def test_save(self):
        """ Change the configuration data, and check that it's correctly saved in a file."""
        my_configuration = Configuration(CONFIG_ABSENT_FILE_NAME)
        my_configuration.set(KEY_FREQ,  DATA_CONFIG_FREQ)
        my_configuration.save()
        
        with open(CONFIG_ABSENT_FILE_NAME,  'r') as data_file:
            my_data = json.load(data_file)
        self.assertEqual(my_data[KEY_FREQ],  DATA_CONFIG_FREQ)
    
def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestConfiguration)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
