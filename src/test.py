################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Unit Tests
#  author:      Stenli Karanxha
#  summary:     Runs all the tests: unit tests and performance tests.
#  revision:    1.1, 16 Jan 2014
################################################################

def run_unit_tests():
    import test_configuration
    test_configuration.run_all(0)

    import test_audioread
    test_audioread.run_all(0)
    
    import test_configuration
    test_configuration.run_all(0)

    import test_database
    test_database.run_all(0)
    
    import test_landmark
    test_landmark.run_all(0)
    
    import test_graphic
    test_graphic.run_all(0)
    
    import test_hash
    test_hash.run_all(0)
    
    import test_micread
    test_micread.run_all(0)
    
    import test_research
    test_research.run_all(0)
    
    import test_Z
    test_Z.run_all(0)

    
def run_performance_tests():
    pass

if __name__ == '__main__':
    run_unit_tests()
    run_performance_tests()
