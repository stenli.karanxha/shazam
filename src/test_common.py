################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Unit Tests
#  author:      Stenli Karanxha
#  summary:     Define the unit tests for the common data structures
#  revision:    1.1, 26 Jan 2014
################################################################

import unittest
import common

class TestCommon(unittest.TestCase):

    def setUp(self):
        self.landmark_1=common.Landmark(10.12345, 1201)
        self.landmark_2=common.Landmark(10.45678, 1373)
        self.rounding=(2, -2) #10 ms, 100Hz
        self.time_shift=0.02  #20 ms
        
    def tearDown(self):
        pass
        
    def test_landmark_string(self):
        """ Tests that the landmarks are correctly transformed into strings. """
        expected_string = "(10.123450,1201.000000)"
        found_string = str(self.landmark_1)
        self.assertEqual(expected_string,  found_string)
        
    def test_landmark_shift(self):
        """ Test the shift operation."""
        expected_time = self.landmark_1.get_time() + self.time_shift
        shifted = self.landmark_1
        shifted.time_shift(self.time_shift)
        self.assertEqual(shifted.get_time(),  expected_time)
        
    def test_landmark_round(self):
        """ Test the rounding operation."""
        expected_time = 10.12
        expected_frequency = 1200
        rounded =  self.landmark_1
        rounded.round(self.rounding)
        self.assertEqual(rounded.get_time(),  expected_time)
        self.assertEqual(rounded.get_frequency(),  expected_frequency)

def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCommon)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
