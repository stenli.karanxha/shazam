################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Research
#  author:      Stenli Karanxha, 
#  summary:     Efficient hash code based research algorithm
#  revision:    1.1, 31 Jan 2014, SK, completed the algorithm.
################################################################

import numpy as np

import common

class ResearchAlgorithm(object):
    
    ############
    # Public interface  
    ############
    
    def __init__(self, min_quality_factor):
        """ Constructor with arguments:
        min_quality_factor - The minimum quality factor for a sample match to be acceptable.
        """
        self._min_quality_factor = min_quality_factor

    def find(self, sample_landmarks, database_landmarks):
        """
        Find a sample of hashed landmarks in a pool of them.
        Input:
            sample_landmarks -- list of Landmark objects, sample to search.
            database_landmarks -- list of LandmarkExt objects, pool of landmarks to search
        Output:
            id -- id of the record that fits better to the sample, None if there is no match.
        """
        # Sorts by time the sample landmarks
        sorted_sample = self._sort_by_time(sample_landmarks)
        # Groups the database landmarks.
        database_groups = self._init_landmark_groups(database_landmarks)
        # Within each group, sorts the landmarks by time.
        for key in database_groups:
            database_groups[key][1] = self._sort_by_time(database_groups[key][1])
        # For each group, calculates the quality coefficient.
        for key in database_groups:
            database_groups[key][0] = self._get_similarity(sorted_sample,  database_groups[key][1])
        # Finds the best group, the one with the best quality coefficient.
        return self._get_best_group(database_groups)
    
    ############
    # Private interface
    ############
    def _init_landmark_groups(self, landmarks):
        """
            Initializes the groups of landmarks. 
            Input: 
                landmarks -- list of LandmarkExt objects.
            Output:
                result: dictionary, with record id-s as keys.
        """
        # Inits the landmarks groups
        result = {}
        for landmark in landmarks:
            id = landmark.id
            if id in result:
                result[id][1].append(landmark)
            else:
                result[id] = (0.0,  [])
        return result
        
    @staticmethod
    def _sort_by_time(landmarks):
        """
        Receive a list of Landmarks and sorts it in order of increasing time.
        """
        return sorted(landmarks, key = lambda landmark: landmark.get_time())

    def _get_similarity(self, base_landmarks, check_landmarks):
        """
        Calculate a similarity coefficient between two groups of Landmark objects. 
        The similarity coefficient is given from the number of check_landmarks 
        which correspond to objects with increasing index inside base_landmarks.
        """
        valid_indexes = 0.0
        last_found = -1
        for landmark in check_landmarks:
            hash = check_landmarks.hash
            found = [i for i, landmark in enumerate(base_landmarks) if landmark.hash == hash]
            if len(found) == 1 and found[0] > last_found:
                last_found = found[0]
                valid_indexes += 1.0
        return valid_indexes / len(check_landmarks)

    def _get_best_group(self,  landmark_groups):
        """ Returns the ID and similarity factor of the best group, if its
       similarity coefficient is  bigger than min_quality_factor, otherwise returns None."""
        selection = (None,  -1)    # id, quality, index
        for key in landmark_groups:
            group = landmark_groups[key]
            if (group[0] > self._min_quality_factor):
                quality = group[0] * len(group[0])
                if quality > selection[1]:
                    selection = (key,  quality)
        return selection[0]
