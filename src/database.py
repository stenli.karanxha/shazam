################################################################
#  org.:            UZH
#  project:         MAT101, Programming in Python.
#                   Z - Shazam
#  component:       Database management
#  author:          Lea Lutz, Regula Baettig, Stenli Karanxha
#  summary:         Different database wrapping classes
#  revision:        1.1, 22 Jan 2014, SK defined code structure and DatabaseInterface.
#                   1.2, 25 Jan 2014, LL & RB completed DatabaseMemory
################################################################

import os
import json

import common

class MetadataKeys(object):
    ALBUM = 'album'
    TITLE = 'title'
    AUTHOR = 'author'
    YEAR = 'year'
    ALL = [ALBUM,  TITLE,  AUTHOR,  YEAR]

class RecordKeys(object):
    METADATA = 'metadata'
    HASHES = 'hashes'
    ALL = [METADATA, HASHES]

def construct_record(metadata, hashes):
    result = {RecordKeys.METADATA: metadata, \
            RecordKeys.HASHES: hashes}
    return result

def is_record_valid(record):
    return all([key in record for key in RecordKeys.ALL]) and \
        record[RecordKeys.METADATA] != None and \
        len(record[RecordKeys.METADATA]) > 0 and \
        record[RecordKeys.HASHES] != None and \
        len(record[RecordKeys.HASHES]) > 0

def record_metadata_to_string(record):
    return json.dumps(record[RecordKeys.METADATA], sort_keys=True, indent=4)


class DatabaseInterface(object):
    """
        Intended as a base class for all the database management classes.
        Member variables: 
        _file_name -- string, full file path of the database
        _is_opened -- boolean, cheks if the database is opened
    """
    
    ############
    # Public interface  
    ############
    
    def __init__(self):
        """ Constructor without arguments
        """
        self._file_name = None
        self._is_opened = False
        
    def open(self,  file_name):
        """ Open the database file. Returns True if successful. """
        self._file_name =  file_name
        self._is_opened = True
        return True
        
    def close(self):
        """ Close the database file. Returns True if successful. """
        self._is_opened = False
        return True
    
    def is_opened(self):
        """ Return True if the database was opened successfully. """
        return self._is_opened
    
    def is_coherent(self):
        """ Return True if the redundant information in the database are coherent. """
        return True
        
    def reset(self):
        """ Reset all the database data. Use with care. """
        assert(self._is_opened)
        return True
        
    def number_of_records(self):
        """ Return the number of records in the database. """
        assert(self._is_opened)
        return 0
    
    def get_records(self):
        """ Returns all the records in the database."""
        assert(self._is_opened)
        return []
    
    def number_of_hash(self):
        """ Return the number of hashed hashes in the database. """
        assert(self._is_opened)
        return 0

    def get_hashes(self):
        """ Returns all the hashes in the database."""
        assert(self._is_opened)
        return []
        
    def add_record(self,  record):
        """ Add a new record in the database. Return the id of the record. """
        assert(self._is_opened)
        return None
        
    def remove_record(self,  record_id):
        """ Remove a record from the database. Return True if the 
        record was in. """
        assert(self._is_opened)
        return True
        
    def get_record(self,  record_id):
        """ Return a record, given the id, or none if it is not in the database. """
        assert(self._is_opened)
        return None
        
    def get_record_ids_with_hash(self,  hash):
        """ Return a list of RecordedHash object, once for each time that the 
        hash is included in the database. """
        assert(self._is_opened)
        return []


class DatabaseKeywords():
    NEXT_UNIQUE_ID = 'next_unique_id'
    RECORD_NUMBER = 'record_number'
    HASH_NUMBER = 'hash_number'
    RECORD_TABLE = 'record_table'
    HASH_TABLE = 'hash_table'
    ALL = [NEXT_UNIQUE_ID, RECORD_NUMBER, HASH_NUMBER, RECORD_TABLE, HASH_TABLE]


class DatabaseMemory(DatabaseInterface):
    """
        Derives and extends all the public interface of DatabaseInterface.
        Simple approach, contains all the data in memory and transforms 
        them into a json data structure to write to file.
        Member variables: 
        _data = dictionary, containing all the data
    """
    
    ############
    # Public interface  
    ############
    
    def __init__(self):
        """ Constructor without arguments.
        Extends the constructor of the base class."""
        super(DatabaseMemory, self).__init__()
        self._data = {}
        
    def open(self,  file_name):
        """ 
        This method opens the database file.
        
        If the path exists and the opening was successful it returns True (sets status ._is_open() to True)
       """
        self._is_opened = False
        self._file_name = file_name
        if os.path.exists(self._file_name):
            with open(self._file_name) as data_file:
                self._data = json.load(data_file)
                self._is_opened = True
        self._complete_missing_fields()

    def close(self):
        """ 
        This method closes the database file.
        
        If the closing was successful it sets the status ._is_open() to False.
       """
        with open(self._file_name, 'w') as outfile:
            json.dump(self._data, outfile, sort_keys=True, indent=4)
        self._is_opened = False
        
    def is_coherent(self):
        """ Return true if the redundant information in the database is coherent. """
        # Coherence of the number of records.
        record_number = self._data[DatabaseKeywords.RECORD_NUMBER]
        counter_records = len(self._data[DatabaseKeywords.RECORD_TABLE])
        if record_number != counter_records:
            return False
        # Coherence of the number of hashes.
        hash_number = self._data[DatabaseKeywords.HASH_NUMBER]
        counter_hash = len(self._data[DatabaseKeywords.HASH_TABLE])
        if hash_number != counter_hash:
            return False
        # Coherence between records and hashes.
        for record_id in self._data[DatabaseKeywords.RECORD_TABLE]:
            for hash in self._data[DatabaseKeywords.RECORD_TABLE][record_id][RecordKeys.HASHES]:
                all_record_ids = self._data[DatabaseKeywords.HASH_TABLE][hash]
                if not record_id in all_record_ids:
                    return False
        return True
        
    def reset(self):
        """This method resets the whole database.
        
        All entries are cleared off the database, and the record number and the hash number are
        set to zero.
        """
        super(DatabaseMemory, self).reset()
        # Resets records
        self._data[DatabaseKeywords.RECORD_TABLE].clear()
        self._data[DatabaseKeywords.RECORD_NUMBER] = 0
        # Resets hashes
        self._data[DatabaseKeywords.HASH_TABLE].clear()
        self._data[DatabaseKeywords.HASH_NUMBER] = 0
        return True
        
    def number_of_records(self):
        """
        This method gets the number of records in the database.
        
        If this method is successful it returns how many records are 
        saved in the database.
        """
        if DatabaseKeywords.RECORD_NUMBER in self._data:
            return self._data[DatabaseKeywords.RECORD_NUMBER]
        else:
            assert(False)   #number of records wasn't in database.
            return None
    
    def get_records(self):
        """
        This method gets all the records in the database.
        """
        return self._data[DatabaseKeywords.RECORD_TABLE]
    
    def number_of_hash(self):
        """ 
        Return the number of hashed hashes in the database. 
        
        If this method is successful it returns how many hashes
        are saved in the database.
        """
        if DatabaseKeywords.HASH_NUMBER in self._data:
            return self._data[DatabaseKeywords.HASH_NUMBER]
        else:
            assert(False)   #number of hash wasn't in database.
            return None
        
    def get_hashes(self):
        """
        This method gets all the hashes in the database.
        """
        return self._data[DatabaseKeywords.HASH_TABLE]
        
    def add_record(self,  record):
        """This method adds a record to the database.
        
        If this method is successful the record and hash number are increased by one
        and the record is saved with a new record id. Also its metadata is saved and
        its hashes (hash informations) are also saved in the hash dictionary.
        """
        #unique id for the record
        record_id = self._get_unique_id()
        #writing the record in the database
        self._data[DatabaseKeywords.RECORD_TABLE][record_id] = record
        #maintaining the coherence within the database
        hash_list=self._data[DatabaseKeywords.RECORD_TABLE][record_id][RecordKeys.HASHES]

        #add the hash_dict to the hash
        for hash in hash_list:
            if hash in self._data[DatabaseKeywords.HASH_TABLE]:
                self._data[DatabaseKeywords.HASH_TABLE][hash].append(record_id)
            else:
                self._data[DatabaseKeywords.HASH_TABLE][hash] = record_id
                self._data[DatabaseKeywords.HASH_NUMBER] +=1

        self._data[DatabaseKeywords.RECORD_NUMBER] += 1
        return record_id
        
        
    def remove_record(self,  record_id):
        """"This method removes a record to the database.
        
        If this method is successful the record and hash number are decreased by one
        and the record deleted the record id. Also its metadata from the record are removed and
         its hashes (hash informations) are deleted from the hash dictionary.
        """
        if record_id in self._data[DatabaseKeywords.RECORD_TABLE]:
            for hash in self._data[DatabaseKeywords.RECORD_TABLE][record_id][RecordKeys.HASHES]:
                #hash has to be there, otherwise the database would not be coherent
                all_record_ids = self._data[DatabaseKeywords.HASH_TABLE][hash]
                all_record_ids.remove(record_id)
                if len(all_record_ids) == 0:
                  del  self._data[DatabaseKeywords.HASH_TABLE][hash]
                  self._data[DatabaseKeywords.HASH_NUMBER] -= 1
            del self._data[DatabaseKeywords.RECORD_TABLE][record_id]
            self._data[DatabaseKeywords.RECORD_NUMBER] -= 1
            return True
        else:
            return False
        
    def get_record(self,  record_id):
        """This method gets a record by its id.
        
        If this method is successful it returns all information of it.
        If the record id does not exist it returns None
        """
        if DatabaseKeywords.RECORD_TABLE in self._data:
            all_records = self._data[DatabaseKeywords.RECORD_TABLE]
            if record_id in all_records:
                return all_records[record_id]
        return None
        
    def get_record_ids_with_hash(self,  hash):
        """ This method returns all the records ids
        associated with a given hash.
        """
        return self._data[DatabaseKeywords.HASH_TABLE][hash]
    
    def get_hashes_for_record_id(self,  record_id):
        """ This method returns all the hashes contained in a given record.
        """
        return self._data[DatabaseKeywords.RECORD_TABLE][record_id][RecordKeys.HASHES]
    

    ############
    # Private interface  
    ############
    def _complete_missing_fields(self):
        """
        Fill the data structure with any missing fields from all those required to define the 
        full json structure.
        """
        if not DatabaseKeywords.RECORD_TABLE in self._data:
            self._data[DatabaseKeywords.RECORD_TABLE] = {}
            self._data[DatabaseKeywords.RECORD_NUMBER] = 0
        if not DatabaseKeywords.HASH_TABLE in self._data:
            self._data[DatabaseKeywords.HASH_TABLE] = {}
            self._data[DatabaseKeywords.HASH_NUMBER] = 0
        if not DatabaseKeywords.RECORD_NUMBER in self._data:
            self._data[DatabaseKeywords.RECORD_NUMBER] = 0
        if not DatabaseKeywords.HASH_NUMBER in self._data:
            self.data[DatabaseKeywords.HASH_NUMBER] = 0

    def _get_unique_id(self):
        id = '%(number)10.0f' % {"number": self._data[DatabaseKeywords.NEXT_UNIQUE_ID]}
        self._data[DatabaseKeywords.NEXT_UNIQUE_ID] += 1
        return id


class DatabaseDisk(DatabaseInterface):
    """
        Derives and extends all the public interface of DatabaseInterface.
        TO BE COMPLETED
    """
    
    ############
    # Public interface  
    ############
    
    def __init__(self):
        """ Constructor without arguments """
        super(DatabaseDisk, self).__init__()
        
    def open(self,  file_name):
        self._is_opened = True
        
    def close(self):
        self._is_opened = False

    def is_coherent(self):
        """ TO BE COMPLETED. """
        pass
        
    def reset(self):
        """ TO BE COMPLETED. """
        pass
        
    def number_of_records(self):
        """ TO BE COMPLETED. """
        pass
    
    def get_records(self):
        """ TO BE COMPLETED. """
        pass
    
    def number_of_hash(self):
        """ TO BE COMPLETED. """
        pass

    def get_hashes(self):
        """ TO BE COMPLETED. """
        pass
        
    def add_record(self,  record):
        """ TO BE COMPLETED. """
        pass
        
    def remove_record(self,  record_id):
        """ TO BE COMPLETED. """
        pass
        
    def get_record(self,  record_id):
        """ TO BE COMPLETED. """
        pass
        
    def get_record_ids_with_hash(self,  hash):
        """ TO BE COMPLETED. """
        pass
