################################################################
#  org.: 	            UZH
#  project: 	        MAT101, Programming in Python.
#		                     Z - Shazam
#  component:	    Main script
#  author: 	        Stenli Karanxha
#  summary:	        Decoded the command line, and defines the execution on this run.
#  revision: 	        1.1, 27 Jan 2014
################################################################

import os
import json

import database

################################################################
#  Global variables
################################################################
g_configuration = None  # Configuration object
g_database = None       # Database object
g_audioread= None       # FFT algorithm.
g_landmark = None       # Landmarking algorithm.
g_research = None       # Reseach lagorithm

################################################################
#  Global constants
################################################################
CONFIGURATION_FILE_NAME = '../files/program_data/config.json'
WAV_FILE_NAME = 'wav_file_name'

################################################################
#  Border functions
################################################################
def start_up():
    """ Initialize and open the database, creates all the algorithm objects. 
    """
    # Reads the configuration file
    import configuration
    global g_configuration
    g_configuration = configuration.Configuration(CONFIGURATION_FILE_NAME)
    g_configuration.load()
    assert(g_configuration.is_valid())
    
    # Initializes and opens the database
    file_name = g_configuration.get(configuration.CDefaults.DATABASE_FILE_NAME[0])
    import database
    global g_database
    g_database = database.DatabaseMemory()
    g_database.open(file_name)
    
    # Initializes the audio transformer algorithm
    fft_window_size = g_configuration.get(configuration.CDefaults.FFT_WINDOW_SIZE[0])
    import audioread
    global g_audioread
    g_audioread = audioread.AudioPiece(fft_window_size)
    
    # Initializes the landmarking algorithm
    quantization = g_configuration.get(configuration.CDefaults.ROUNDING_FACTOR[0])
    block_size =   g_configuration.get(configuration.CDefaults.BLOCK_SIZE[0])
    average_density = g_configuration.get(configuration.CDefaults.AVERAGE_DENSITY[0])
    import landmark
    global g_landmark
    g_landmark = landmark.LandmarkAlgorithm(quantization,  block_size,  average_density)
    
    # Initializes the research algorithm
    min_quality_factor = g_configuration.get(configuration.CDefaults.MIN_QUALITY_FACTOR[0])
    import research
    global g_research
    g_research = research.ResearchAlgorithm(min_quality_factor)
    
   
def shut_down():
    """ Closes the database. Tearing down the algorithm objects is not needed."""
    if g_database:
        g_database.close()

################################################################
#  Shazam commands
################################################################
def reset_database():
    """ Reset all the data in the database. """
    return g_database.reset()
    
def load_songs(input_songs_definition_file):
    """ Given a list of songs written as a database records inside the input_songs_definition_file,
    generate and save corresponding records inside the database. """
    # Opens and reads the input records file.
    input_songs_definitions = {}
    if os.path.exists(input_songs_definition_file):
        with open(input_songs_definition_file,  'r') as data_file:
            input_songs_definitions = json.load(data_file)
    # Stops elaboration in case of no songs.
    tot_songs = len(input_songs_definitions)
    successful_songs = 0            
    if tot_songs == 0:
        return (tot_songs,  successful_songs)
    # Produces a record for each definition, and writes it in the database
    for song_key in input_songs_definitions:
        # Reads the input song definition
        definition = input_songs_definitions[song_key]
        if not _is_definition_valid(definition):
            continue
        # Prepares the metadata of the record
        metadata = {}
        for metadata_key in database.MetadataKeys.ALL:
            if metadata_key in definition:
                metadata[metadata_key] = definition[metadata_key]
        # Prepares the landmarks of the record
        discrete_spectrum = g_audioread.calculate_fft(definition[WAV_FILE_NAME])
        landmarks = g_landmark.calculate_landmarks(discrete_spectrum)
        hashes = [landmark.hash for landmark in landmarks]
        # Prepares the record and saves it in the database
        my_record = database.construct_record(metadata,  hashes)
        if my_record.is_valid():
            g_database.add_record(my_record)
            successful_songs += 1
    return (tot_songs,  successful_songs)

def _is_definition_valid(input_song_definition):
    """ Check if a song definition is valid. Namely, it contains a wav file name, and
    this points to a valid file."""
    if not WAV_FILE_NAME in input_song_definition:
        return False
    wav_file_name = input_song_definition[WAV_FILE_NAME]
    if not os.path.exists(wav_file_name):
        return False
    return True
    
def delete_song(record_id):
    """ Delete from the database the song with the given record ID."""
    return g_database.remove_record(record_id)

def research_song(wav_file_name):
    """ Look for a song in the database corresponding to the sound sample presented as a .wav file."""
    # Checks that the given file is valid.
    if not os.path.exists(wav_file_name):
        return None
    # Gets the landmarks of the audio samples.
    discrete_spectrum = g_audioread.calculate_fft(definition[WAV_FILE_NAME])
    sample_landmarks = g_landmark.calculate_landmarks(discrete_spectrum)
    if len(sample_landmarks) == 0:
        return None
    # Gets from the database all the landmarks that have the same hash as the sample landmarks.
    database_landmarks = []
    for landmark in sample_landmarks:
        add_landmarks = [common.LandmarkExt(landmark, id) for id in g_database.get_record_ids_with_hash(landmark.hash)] 
        database_landmarks.extend(add_landmarks)
    # Asks the research algorithm to come up with the ID of the record with the best correspondence.
    record_id = g_research.find(sample_landmarks, database_landmarks)
    if not record_id:
        return None
    # Ask the database to come up with the corresponding record
    return g_database.get_record(record_id)
    

################################################################
#  Main, together with command line parsing.
################################################################

if __name__ == '__main__':
    
    # Initializes the database and the algorithms
    start_up()
   
   # Prepares and runs the argument parsing
    import argparse
    parser = argparse.ArgumentParser(description='Run the Shazam implementation, either client or server side.')
    parser.add_argument("--reset", help="Reset the song database.",  action="store_true")
    parser.add_argument("-l", "--load", help="Load a list songs written in a definition file to the database.",  type=str)
    parser.add_argument("-d", "--delete", help="Delete songs from the database, give a list of valid track ids.",  type=str)
    parser.add_argument("-f", "--find", help="Find the song of which a part is contained in a wav file.",  type=str)
    args = parser.parse_args()
    
    # Reset database
    if args.reset:
        print args.reset
        result = reset_database()
        if result:
            print "Database reset succeeded."
        else:
            print "Database reset failed."        

    # Load songs in the database
    if args.load:
        print args.load
        result = load_songs(args.load)
        if result == (0, 0):
            print "No songs added to the database."
        else:
            print 'Added to the database %(acceptable).0f songs of a total of %(total).0f' % {"acceptable": result[1], "total": result[0]}

    # Delete songs from the database
    if args.delete:
        print args.delete
        result = delete_song(args.delete)
        if result:
            print "Deleting the song succeeded."
        else:
            print "Deleting the song failed."

    # Given a song part find to which song it belongs
    if args.find:
        print args.find
        result = find_song_part(args.find)
        if result:
            print database.record_metadata_to_string(result)
        else:
            print "No correspondence was found in the database."
    
    # Closes the database
    shut_down()
