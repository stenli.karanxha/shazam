################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Fingerprinting
#  author:      Antonio De la Torre, Stenli Karanxha,
#  summary:     Algorithm, generating a hash based on two SpectrumPeaks.
#  revision:    1.1, 22 Jan 2014, SK defined interface
#               1.2, 26 Jan 2014, AT completed the hash calculation
################################################################

import numpy as np
import copy
import binascii

import common

class HashAlgorithm(object):
    
    ############
    # Public interface  
    ############
    
    def __init__(self, rounding_factor):
        """ Constructor with argument:
            rounding_factor -- Maximum number of digits allowed for time and frequency.
        """
        self._rounding_factor = rounding_factor

    def calculate_hash(self, peak_1, peak_2):
        """
        Calculate the hash of two SpectrumPeaks.
        Input:
        peak_1, peak_2 -- SpectrumPeak objects.
        Output:
        hash -- integer, hash value of the combined SpectrumPeak objects.
        """
        # Round the result to delete information smaller than the quantization.
        rounded_peak_1 = copy.deepcopy(peak_1)
        rounded_peak_1.round(self._rounding_factor)
        rounded_peak_2 = copy.deepcopy(peak_2)
        rounded_peak_2.round(self._rounding_factor)
        # Calculates the hash
        peak_difference = self.prepare_string(rounded_peak_1,  rounded_peak_2)
        hash = binascii.crc32(str(peak_difference))
        return hash
        
    ############
    # Private interface  
    ############
    @staticmethod
    def prepare_string(peak_1,  peak_2):
        time = abs(peak_1.get_time() - peak_2.get_time())
        freq1 = max(peak_1.get_frequency(), peak_2.get_frequency())
        freq2 = min(peak_1.get_frequency(), peak_2.get_frequency())
        return '(%(time).12f,%(freq1).12f, %(freq2).12f)' % {"time": time, "freq1": freq1, "freq2": freq2}

