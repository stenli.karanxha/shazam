################################################################
#  org.: 	            UZH
#  project: 	        MAT101, Programming in Python.
#		                     Z - Shazam
#  component:	    Unit Tests
#  author: 	        Stenli Karanxha, Xenia Ohmer
#  summary:	        Define the unit tests for the fft calculation of an audio file.
#  revision: 	        1.1, 20 Jan 2014
################################################################

import audioread
import unittest

import numpy as np
import json
import os

class TestAudioread(unittest.TestCase):

    def setUp(self):
        pass
        
    def tearDown(self):
        pass
        
    def test_invalid_wav(self):
        self.assertTrue(True)

    def test_mono_01_characteristics(self):
        """ Test characteristics of a monotone audio sample: 10 kHz for 0.5 s, sample rate 44.1 kHz """
        self._check_characteristics('mono01')

    def test_mono_02_characteristics(self):
        """ Test characteristics of a monotone audio sample: 12 kHz for 0.5 s, sample rate 48 kHz """
        self._check_characteristics('mono02')

    def test_mono_01_fft(self):
        """ Test fft output from a monotone audio sample: 10 kHz for 0.5 s, sample rate 44.1 kHz """
        self._check_fft('mono01')

    def test_mono_02_fft(self):
        """ Test characteristics of a monotone audio sample: 12 kHz for 0.5 s, sample rate 48 kHz """
        self._check_fft('mono02')

    def test_sweep_01_characteristics(self):
        """ Test characteristics of a sweep audio sample: 500 Hz to 15 kHz linearly in  10 s, sample rate 44.1 kHz """
        self._check_characteristics('sweep01')

    def test_sweep_02_characteristics(self):
        """ Test characteristics of a sweep audio sample: 500 Hz to 15 kHz logarithmic in  10 s, sample rate 48 kHz """
        self._check_characteristics('sweep02')

    def test_sweep_01_fft(self):
        """ Test fft of a sweep audio sample: 500 Hz to 15 kHz linearly in  10 s, sample rate 44.1 kHz """
        self._check_fft('sweep01')

    def test_sweep_02_fft(self):
        """ Test fft of a sweep audio sample: 500 Hz to 15 kHz logarithmic in  10 s, sample rate 48 kHz """
        self._check_fft('sweep02')

    def test_chirp_01_characteristics(self):
        """ Test characteristics of a chirp audio sample: 500 Hz to 15 kHz linearly in  0.01 s, sample rate 44.1 kHz """
        self._check_characteristics('chirp01')

    def test_chirp_02_characteristics(self):
        """ Test characteristics of a sweep audio sample: 500 Hz to 15 kHz logarithmic in  0.01 s, sample rate 48 kHz """
        self._check_characteristics('chirp02')

    def test_chirp_01_fft(self):
        """ Test fft of a chirp audio sample: 500 Hz to 15 kHz linearly in  0.01 s, sample rate 44.1 kHz """
        self._check_fft('chirp01')

    def test_chirp_02_fft(self):
        """ Test fft of a chirp audio sample: 500 Hz to 15 kHz logarithmic in  0.01 s, sample rate 48 kHz """
        self._check_fft('chirp02')

    def _check_characteristics(self,  base_file_name):
        """ Test characteristics of a generic audio file."""
        self._prepare_files(base_file_name)
        if 'chirp' in base_file_name:
            audio = audioread.AudioPiece(fft_windowsize=128)
        else:
            audio = audioread.AudioPiece()       
        audio.calculate_fft(self.audio_file_name)
        json_results = open(self.result_file_name)
        results = json.load(json_results)
        self.assertEqual(results['samplerate'],audio.samplerate, 'incorrect samplerate')
        self.assertEqual(results['samplecount'],audio.samplecount,'incorrect number of samples')
        json_results.close()
        
    def _check_fft(self,  base_file_name):
        """ Test fft of a generic audio file."""
        self._prepare_files(base_file_name)
        if 'chirp' in base_file_name:
            audio = audioread.AudioPiece(fft_windowsize=128)
        else:
            audio = audioread.AudioPiece()
        fft = audio.calculate_fft(self.audio_file_name)
        self.assertIsNotNone(fft,  'No results from the audio read algorithm.')
        json_results = open(self.result_file_name)
        results = json.load(json_results)
        json_fft = results['fft_matrix']
        self.assertTrue(np.allclose(json_fft,fft), 'Incorrect fft_matrix.')
        json_results.close()
    
    def _prepare_files(self,  base_file_name):
        self.audio_file_name = self._get_audio_file_name(base_file_name)
        self.assertTrue(os.path.exists(self.audio_file_name))
        self.result_file_name = self._get_result_file_name(base_file_name)
        self.assertTrue(os.path.exists(self.result_file_name))    
    
    @staticmethod
    def _get_audio_file_name(base_file_name):
        return '../files/unit_test_data/' + str(base_file_name) + '.wav'
    
    @staticmethod
    def _get_result_file_name(base_file_name):
        return '../files/unit_test_data/' + str(base_file_name) + '.json'

def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAudioread)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
