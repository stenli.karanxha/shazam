################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Common
#  author:      Stenli Karanxha, Antonio De la Torre
#  summary:     Contains the common data structures used in the 
#                   interface between classes.
#  revision:    1.1, 22 Jan 2014, SK defined the common data structure classes.
#                       1.2, 25 Jan 2014, AT added operations and functions of the Landmark class.
################################################################

class SpectrumPeak(object):
    """
        Data structure class, with the member variables:
        time -- instant of the dicrete spectrum point.
        frequency -- frequency of the dicrete spectrum point.
        Allows also some simple operations on the data, to support the generation of the hash.
    """
    
    def __init__(self, time, frequency):
        """
        Constructor with arguments:
        time - float, time in s of the landmark
        frequency - float, frequency in Hz of the landmark
        """
        self.time = time
        self.frequency = frequency
        
    def __str__(self):
        return '(%(time).6f,%(frequency).6f)' % {"time": self.time, "frequency": self.frequency}
        
    def get_time(self):
        return self.time
        
    def get_frequency(self):
        return self.frequency        

    def time_shift(self,  shift):
        self.time += shift
        
    def round(self, rounding_factor):
        self.time = round(self.time,  rounding_factor[0])
        self.frequency = round(self.frequency,  rounding_factor[1])


class Landmark(object):
    """
        Data structure class, with the member variables:
        hash -- integer, hash value.
        time -- float, time in s
    """
    def __init__(self, time, hash):
        self.time = time
        self.hash = hash
        
    def __str__(self):
        return '(%(time)f,%(hash).0f)' % {"time": self.time, "hash": self.hash}


class LandmarkExt(Landmark):
    """
        Extension of the Landmark data structure with the extra member variable:
        record_ID -- ID of the record with the given landmark.
    """
    
    def __init__(self,  landmark,  id):
        super(LandmarkExt, self).__init__(landmark.time, landmark.hash)
        self.id = id
        
    def __str__(self):
        return '(%(time)f,%(hash).0f, %(id).0f)' % {"time": self.time, "hash": self.hash,  "id": self.id}
        
