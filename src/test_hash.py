################################################################
#  org.:        UZH
#  project:     MAT101, Programming in Python.
#               Z - Shazam
#  component:   Unit Tests
#  author:      Stenli Karanxha
#  summary:     Define the unit tests for the hashing of the fingerprints
#  revision:    1.1, 26 Jan 2014
################################################################

import unittest
import hash
import common

class TestHash(unittest.TestCase):

    def setUp(self):
        self.random_peak_1=common.SpectrumPeak(0.001, 1200)
        self.random_peak_2=common.SpectrumPeak(0.003, 1370)
        self.rounding=(2, -2)  # time approximated to 10 ms, frequency to 100 Hz
        self.small_shift = 0.001 #1 ms, 10Hz
        self.big_shift = 5.4 # 5.4 s
        self.hash_algorithm = hash.HashAlgorithm(self.rounding)
        
    def tearDown(self):
        self.hash_algorithm=None
        
    def test_algorithm_running(self):
        hash_value = self.hash_algorithm.calculate_hash(self.random_peak_1, self.random_peak_2 )
        self.assertIsNotNone(hash_value)
        
    def test_quantization_Invariance(self):
        """The hash-value should not change if one of the peaks change by an 
        amount smaller than the quantization."""
        original_hash=self.hash_algorithm.calculate_hash(self.random_peak_1,self.random_peak_2)
        self.random_peak_1.time_shift(self.small_shift)
        shifted_hash=self.hash_algorithm.calculate_hash(self.random_peak_1,self.random_peak_2 )
        self.assertEqual(original_hash, shifted_hash)
        
    def test_translation_invariance(self):
        """ Tests that the generated hash is the same if both peaks are translated in time."""
        original_hash=self.hash_algorithm.calculate_hash(self.random_peak_1,self.random_peak_2)
        self.random_peak_1.time_shift(self.big_shift)
        self.random_peak_2.time_shift(self.big_shift)
        shifted_hash=self.hash_algorithm.calculate_hash(self.random_peak_1,self.random_peak_2 )
        self.assertEqual(original_hash, shifted_hash)

    def test_sign_invariance(self):
        """ Tests that the generated hash changes if the 
        order of the peaks is inverted."""
        original_hash=self.hash_algorithm.calculate_hash(self.random_peak_1,self.random_peak_2)
        inverted_hash=self.hash_algorithm.calculate_hash(self.random_peak_2,self.random_peak_1)
        self.assertEqual(original_hash, inverted_hash)

def run_all(_verbosity = 2):
    suite = unittest.TestLoader().loadTestsFromTestCase(TestHash)
    unittest.TextTestRunner(verbosity=_verbosity).run(suite)    

if __name__ == '__main__':
    run_all()
